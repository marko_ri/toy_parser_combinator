defmodule ToyParserCombinatorTest do
  use ExUnit.Case

  test "greets the world" do
    expected =
      {:ok,
       %{
         statement: :select,
         columns: ["col1"],
         from: %{
           statement: :select,
           columns: ["col2", "col3"],
           from: %{
             statement: :select,
             columns: ["col4", "col5", "col6"],
             from: "some_table"
           }
         }
       }, ""}

    assert ToyParserCombinator.run() == expected
  end
end
