Toy parser combinator in Elixir, based on the video: [Parsing from first principles](https://www.youtube.com/watch?v=xNzoerDljjo).
